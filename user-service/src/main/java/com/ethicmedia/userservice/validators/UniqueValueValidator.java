package com.ethicmedia.userservice.validators;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
public class UniqueValueValidator implements ConstraintValidator<UniqueValue, Object> {


    private final JdbcTemplate jdbcTemplate;

    private String tableName;
    private String columnName;

    @Override
    public void initialize(UniqueValue constraintAnnotation) {
        this.tableName = constraintAnnotation.tableName();
        this.columnName = constraintAnnotation.columnName();

    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        // Perform the uniqueness check using JDBC or your preferred method
        String query = "SELECT COUNT(*) FROM " + tableName + " WHERE " + columnName + " = ?";
        int count = jdbcTemplate.queryForObject(query, Integer.class, value);

        return count == 0; // If count is 0, the value is unique
    }
}
