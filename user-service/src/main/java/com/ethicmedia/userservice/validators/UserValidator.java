package com.ethicmedia.userservice.validators;


import com.ethicmedia.userservice.dtos.UserRequestDTO;
import org.passay.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserValidator {

    /**
     * ^[a-zA-Z0-9]      # start with an alphanumeric character
     * (                 # start of (group 1)
     * [._](?![._])  # follow by a dot, hyphen, or underscore, negative lookahead to
     * # ensures dot, hyphen, and underscore does not appear consecutively
     * |               # or
     * [a-zA-Z0-9]     # an alphanumeric character
     * )                 # end of (group 1)
     * {3,18}            # ensures the length of (group 1) between 3 and 18
     * [a-zA-Z0-9]$      # end with an alphanumeric character
     * <p>
     * # {4,10} plus the first and last alphanumeric characters,
     * # total length became {5,20}
     */
    private static final String USERNAME_PATTERN =
            "^[a-zA-Z]([._](?![._])|[a-zA-Z0-9]){4,6}[a-zA-Z0-9]$";

    /**
     * This method
     *
     * @param userRequest
     * @return
     */
    public static List<Map<String, String>> validate(final UserRequestDTO userRequest) {

        List<Map<String, String>> errors = new ArrayList<>();
        Map<String, String> map = new HashMap<>();


        if (userRequest == null) {
            map.put("*", "please fill in all the required fields");
            errors.add(map);
            return errors;
        }


        if (userRequest.getPassword() == null) {
            map.put("password", "password is required");

        } else if (validatePassword(userRequest.getPassword()).size() > 0) {
            map.put("password", validatePassword(userRequest.getPassword()).toString());
        }

        if (userRequest.getUsername() == null) {
            map.put("username", "username is required");

        } else if (!validateUsername(userRequest.getUsername())) {
            map.put("username", "username does not meet the requirements");
        }

        if (!map.isEmpty()) {
            errors.add(map);
        }
        return errors;
    }

    public static List<String> validatePassword(String password) {
        List<Rule> rules = new ArrayList<>();
        //Rule 1: Password length should be in between
        //8 and 16 characters
        rules.add(new LengthRule(8, 16));
        //Rule 2: No whitespace allowed
        rules.add(new WhitespaceRule());
        //Rule 3.a: At least one Upper-case character
        rules.add(new CharacterRule(EnglishCharacterData.UpperCase, 1));
        //Rule 3.b: At least one Lower-case character
        rules.add(new CharacterRule(EnglishCharacterData.LowerCase, 1));
        //Rule 3.c: At least one digit
        rules.add(new CharacterRule(EnglishCharacterData.Digit, 1));
        //Rule 3.d: At least one special character
        rules.add(new CharacterRule(EnglishCharacterData.Special, 1));

        PasswordValidator validator = new PasswordValidator(rules);
        PasswordData passwordData = new PasswordData(password);
        RuleResult result = validator.validate(passwordData);

        return validator.getMessages(result);
    }

    public static boolean validateUsername(String username) {
        Pattern pattern = Pattern.compile(USERNAME_PATTERN);
        Matcher matcher = pattern.matcher(username);
        return matcher.matches();
    }


}
