package com.ethicmedia.userservice.models;

import com.ethicmedia.userservice.validators.UniqueValue;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "em_user")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "em_username", length = 20, unique = true, nullable = false)
    //@UniqueValue(tableName = "em_user", columnName = "em_username", message = "username already exists")
    private String username;

    @Column(name = "em_password", length = 200, nullable = false)
    private String password;

}