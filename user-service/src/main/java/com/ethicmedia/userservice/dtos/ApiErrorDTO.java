package com.ethicmedia.userservice.dtos;

import com.ethicmedia.userservice.enums.ErrorCodes;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class ApiErrorDTO {

    private String guid;
    private final String status = "error";
    private ErrorCodes errorCode;
    private String message;
    private int statusCode;
    private String statusName;
    private String path;
    private String method;
    private LocalDateTime timestamp;
    private List errors;

}