package com.ethicmedia.userservice.dtos;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

/**
 * Cette classe à pour but de gérer et formatter les réponses aux client
 * @author jean-claude.mbeng
 *
 */
public class ApiResponseDTO {

    /**
     * Cette méthode statique construire une réponse.
     *
     * @param message        Message à renvoyer
     * @param httpStatus     Status Http de la réponse
     * @param responseObject Données à renvoyer
     * @return Objet de type ResponseEntity
     */
    public static ResponseEntity<Object> responseBuilder(final String status,
                                                         final String message,
                                                         final HttpStatus httpStatus,
                                                         final int httpStatusCode,
                                                         final Object responseObject) {
        Map<String, Object> response = new HashMap<>();


        response.put("status", status);
        response.put("message", message);
        response.put("httpStatus", status);
        response.put("httpStatusCode", httpStatusCode);
        response.put("data", responseObject);

        return new ResponseEntity<>(response, httpStatus);
    }
}
