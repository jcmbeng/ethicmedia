package com.ethicmedia.userservice.dtos;

import com.ethicmedia.userservice.enums.ErrorCodes;
import com.ethicmedia.userservice.exceptions.ApplicationException;
import com.ethicmedia.userservice.models.User;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.*;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
public class UserRequestDTO {

    @NotBlank(message = "username is required.")
    private String username;

    @NotEmpty(message = "password is required.")
    private String password;

    public static User fromDtoToEntity (UserRequestDTO requestDTO) {
        if (requestDTO == null) {

            throw new ApplicationException (
                    ErrorCodes.USER_NOT_VALID,
                    "An internal server error occurred",
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }

        return User.builder ()
                .username (requestDTO.getUsername ())
                .password (requestDTO.getPassword ())
                .build ();
    }
}
