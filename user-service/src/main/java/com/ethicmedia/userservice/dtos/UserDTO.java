package com.ethicmedia.userservice.dtos;


import com.ethicmedia.userservice.exceptions.ApplicationException;
import com.ethicmedia.userservice.models.User;
import com.ethicmedia.userservice.enums.ErrorCodes;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UserDTO {


    private static String apiPath;
    private Long id;
    private String username;
    private String href;


    public static UserDTO fromEntityToDTO (User user, String apiPath) {

        if (user == null) {

            throw new ApplicationException (
                    ErrorCodes.USER_NOT_VALID,
                    "An internal error occurred",
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }
        return UserDTO.builder ()
                .id (user.getId ())
                .username (user.getUsername ())
                .href (apiPath + "/" + user.getId ())
                .build ();
    }
}


