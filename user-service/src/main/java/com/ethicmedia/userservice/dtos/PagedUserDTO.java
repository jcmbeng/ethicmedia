package com.ethicmedia.userservice.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class PagedUserDTO
{
   private List<UserDTO> users;

    private int currentPage;

    private  long totalItems;

    private int totalPages;
}
