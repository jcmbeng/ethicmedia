package com.ethicmedia.userservice.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@Builder

public class ErrorRequestDTO {

    private final List<Map<String, String>> errors;

}