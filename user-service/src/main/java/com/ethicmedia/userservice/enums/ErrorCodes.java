package com.ethicmedia.userservice.enums;

public enum ErrorCodes {

    // ACCOUNT ERROR CODE
    USER_NOT_FOUND (404),
    USER_REQUEST_ERROR (400),
    INTERNAL_ERROR (500),
    USER_NOT_VALID (501),
    INVALID_ID_SUPPLIED (1000), USERNAME_ALREADY_USED (1002);


    private int code;

    ErrorCodes (int code) {
        this.code = code;
    }

    public int getCode () {
        return code;
    }
}
