package com.ethicmedia.userservice.enums;

public enum AccountStatus {
    ACTIVE,
    BLOCKED,
    DORMANT,
    CLOSED,
    INACTIVE,
    NOT_VALIDATED,
    OPEN,
}
