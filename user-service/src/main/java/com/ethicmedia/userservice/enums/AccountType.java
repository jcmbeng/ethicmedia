package com.ethicmedia.userservice.enums;

public enum AccountType
{
    PERSONAL, BUSINESS
}
