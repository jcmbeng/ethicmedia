package com.ethicmedia.userservice.handlers;


import com.ethicmedia.userservice.dtos.ApiErrorDTO;
import com.ethicmedia.userservice.enums.ErrorCodes;
import com.ethicmedia.userservice.exceptions.ApplicationException;
import com.ethicmedia.userservice.exceptions.InputRequestException;
import com.ethicmedia.userservice.exceptions.UserNotFoundException;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@RestControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ApplicationExceptionHandler extends ResponseEntityExceptionHandler {

    private static final String ERROR_MESSAGE_WITH_GUID = "\nError GUID=%s; error message: %s";
    private static final String ERROR_MESSAGE_MORE_DETAILS = " An error occurred status code %s For more details check the logs with the GUID.";

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<Object> userNotFoundException(final UserNotFoundException exception,
                                                        final HttpServletRequest request) {
        var guid = UUID.randomUUID().toString();

        log.error(String.format(ERROR_MESSAGE_WITH_GUID, guid, exception.getMessage()), exception);

        ApiErrorDTO apiErrorDTO = apiErrorDTO(guid,
                "Resource not found.",
                HttpStatus.NOT_FOUND.value(),
                HttpStatus.NOT_FOUND.name(),
                ErrorCodes.USER_NOT_FOUND,
                request.getRequestURI(),
                request.getMethod(),
                LocalDateTime.now(), null);

        return new ResponseEntity<>(apiErrorDTO, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ApplicationException.class)
    public ResponseEntity<Object> handleApplicationException(final ApplicationException exception,
                                                             final HttpServletRequest request) {
        var guid = UUID.randomUUID().toString();

        log.error(String.format(ERROR_MESSAGE_WITH_GUID, guid, exception.getMessage()), exception);

        ApiErrorDTO apiErrorDTO = apiErrorDTO(guid,
                exception.getMessage() + ".\n For more details check " + "the logs with the GUID.",
                exception.getHttpStatus().value(),
                exception.getHttpStatus().name(),
                exception.getErrorCode(),
                request.getRequestURI(),
                request.getMethod(),
                LocalDateTime.now(), null);

        return new ResponseEntity<>(apiErrorDTO, exception.getHttpStatus());
    }

    @ExceptionHandler(InputRequestException.class)
    public ResponseEntity<Object> handleApplicationException(final InputRequestException exception,
                                                             final HttpServletRequest request) {
        var guid = UUID.randomUUID().toString();
        log.error(String.format(ERROR_MESSAGE_WITH_GUID, guid, exception.getMessage()), exception);
        ApiErrorDTO apiErrorDTO = apiErrorDTO(guid,
                " Request failed with status code " + HttpStatus.BAD_REQUEST.value() + ".\n For more details check " + "the logs with the GUID.",
                HttpStatus.BAD_REQUEST.value(),
                HttpStatus.BAD_REQUEST.name(),
                exception.getErrorCode(),
                request.getRequestURI(),
                request.getMethod(),
                LocalDateTime.now(),
                exception.getErrors());

        return new ResponseEntity<>(apiErrorDTO, HttpStatus.BAD_REQUEST);


    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleUnknownException(final Exception exception, final HttpServletRequest request) {
        var guid = UUID.randomUUID().toString();
        log.error(String.format(ERROR_MESSAGE_WITH_GUID, guid, exception.getMessage()), exception);

        ApiErrorDTO apiErrorDTO = apiErrorDTO(guid,
                " An error occurred status code " + HttpStatus.INTERNAL_SERVER_ERROR.value() + ".\n For more details check " + "the logs with the GUID.",
                HttpStatus.INTERNAL_SERVER_ERROR.value(),
                HttpStatus.INTERNAL_SERVER_ERROR.name(),
                ErrorCodes.INTERNAL_ERROR, request.getRequestURI(),
                request.getMethod(),
                LocalDateTime.now(),
                Collections.singletonList("An unknown error occurred"));

        return new ResponseEntity<>(apiErrorDTO, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception, HttpHeaders headers, HttpStatusCode status, WebRequest request) {


        var guid = UUID.randomUUID().toString();
        log.error(String.format(ERROR_MESSAGE_WITH_GUID, guid, exception.getMessage()), exception);

        List<String> errors = exception.getBindingResult().getFieldErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.toList());

        HttpServletRequest servletRequest = ((ServletWebRequest) request).getRequest();

        ApiErrorDTO apiErrorDTO = apiErrorDTO(guid,
                " Request failed with status code " + HttpStatus.BAD_REQUEST.value() + ".\n For more details check " + "the logs with the GUID.",
                HttpStatus.BAD_REQUEST.value(),
                HttpStatus.BAD_REQUEST.name(),
                ErrorCodes.USER_REQUEST_ERROR,
                servletRequest.getRequestURI(),
                servletRequest.getMethod(),
                LocalDateTime.now(),
                errors);

        return new ResponseEntity<>(apiErrorDTO, HttpStatus.BAD_REQUEST);
    }


    private ApiErrorDTO apiErrorDTO(String guid, String message, int statusCode, String statusName, ErrorCodes errorCode, String path, String method, LocalDateTime timestamp, List errors) {
        return ApiErrorDTO.builder()
                .guid(guid)
                .errorCode(errorCode)
                .message(message)
                .statusCode(statusCode)
                .statusName(statusName)
                .path(path)
                .method(method)
                .timestamp(timestamp)
                .errors(errors)
                .build();
    }
}
