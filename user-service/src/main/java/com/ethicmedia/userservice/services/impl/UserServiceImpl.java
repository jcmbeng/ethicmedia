package com.ethicmedia.userservice.services.impl;

import com.ethicmedia.userservice.dtos.PagedUserDTO;
import com.ethicmedia.userservice.dtos.UserDTO;
import com.ethicmedia.userservice.dtos.UserRequestDTO;
import com.ethicmedia.userservice.enums.ErrorCodes;
import com.ethicmedia.userservice.exceptions.ApplicationException;
import com.ethicmedia.userservice.exceptions.InputRequestException;
import com.ethicmedia.userservice.exceptions.UserNotFoundException;
import com.ethicmedia.userservice.models.User;
import com.ethicmedia.userservice.repositories.UserRepository;
import com.ethicmedia.userservice.services.UserService;
import com.ethicmedia.userservice.validators.UserValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.*;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    @Value("${api.config.path}")
    private String apiPathValue;

    /**
     * Recherche un User avec son id
     *
     * @param id
     * @return UserDTO User convertit via sa DTO
     * @throws ApplicationException {@link Exception}
     */
    @Transactional(timeout = 10, readOnly = true)
    @Override
    public UserDTO findById(Long id) throws ApplicationException {
        return userRepository.findById(id)
                .map(
                        (User user) -> UserDTO.fromEntityToDTO(user, apiPathValue)
                ).orElseThrow(UserNotFoundException::new);
    }

    @Transactional(readOnly = true, timeout = 10)
    @Override
    public PagedUserDTO findAll(Pageable pagingSort) throws ApplicationException {

        List<UserDTO> userDTOS;

        Page<User> pageUsers = userRepository.findAll(pagingSort);

        userDTOS = pageUsers
                .stream()
                .map(user -> UserDTO.fromEntityToDTO(user, apiPathValue))
                .toList();

        return PagedUserDTO.builder()
                .currentPage(pageUsers.getNumber())
                .totalPages(pageUsers.getTotalPages())
                .totalItems(pageUsers.getTotalElements())
                .users(userDTOS)
                .build();
    }


    @Override
    @Transactional(readOnly = true, timeout = 10)
    public UserDTO findByUsername(String username) throws ApplicationException {
        return UserDTO.fromEntityToDTO(
                userRepository.findByUsername(username).orElseThrow(UserNotFoundException::new), apiPathValue
        );
    }

    @Transactional(timeout = 10)
    @Override
    public UserDTO create(UserRequestDTO request) throws ApplicationException {

        List<Map<String, String>> errors = UserValidator.validate(request);

        if (!errors.isEmpty()) {
            throw new InputRequestException(
                    ErrorCodes.USER_NOT_VALID,
                    "Bad request provided, check the submitted data and try again.",
                    errors
            );
        }

        if (userRepository.existsUserByUsername(request.getUsername())) {
            Map<String, String> map = new HashMap<>();
            map.put("username", "username is already used");

            throw new InputRequestException(
                    ErrorCodes.USERNAME_ALREADY_USED,
                    "Duplicate data found in request.",
                    Collections.singletonList(map)
            );
        }

        return UserDTO.fromEntityToDTO(
                userRepository.save(
                        UserRequestDTO.fromDtoToEntity(
                                request
                        )
                ), apiPathValue
        );
    }

    @Transactional(timeout = 10)
    @Override
    public UserDTO update(Long id, UserRequestDTO request) throws ApplicationException {
        List<Map<String, String>> errors = UserValidator.validate(request);

        if (!errors.isEmpty()) {
            throw new InputRequestException(
                    ErrorCodes.INVALID_ID_SUPPLIED,
                    "Bad request provided, check the submitted data and try again.",
                    errors
            );
        }

        User user = userRepository.findById(id).orElseThrow(UserNotFoundException::new
        );

        Optional<User> optionalUserToCheck = userRepository.findByUsername(request.getUsername());

        if (optionalUserToCheck.isPresent() && !optionalUserToCheck.get().getId().equals(user.getId())) {
            throw new ApplicationException(
                    ErrorCodes.USERNAME_ALREADY_USED,
                    "Duplicate username found.",
                    HttpStatus.INTERNAL_SERVER_ERROR
            );
        }

        user.setUsername(request.getUsername());
        user.setPassword(request.getPassword());
        return UserDTO.fromEntityToDTO(userRepository.save(user), apiPathValue);

    }

    @Override
    public void delete(Long id) throws ApplicationException {

        User user = userRepository.findById(id).orElseThrow(
                UserNotFoundException::new
        );

        userRepository.delete(user);
    }


}
