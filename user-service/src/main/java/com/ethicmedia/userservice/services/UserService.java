package com.ethicmedia.userservice.services;

import com.ethicmedia.userservice.dtos.PagedUserDTO;
import com.ethicmedia.userservice.dtos.UserDTO;
import com.ethicmedia.userservice.dtos.UserRequestDTO;
import com.ethicmedia.userservice.exceptions.ApplicationException;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Interface de l'application service des utilisateurs
 *
 * @author jean-claude.mbeng
 */
public interface UserService {
    /**
     * Recherche un User avec son id
     *
     * @param id
     * @return UserDTO User convertit via sa DTO
     * @throws ApplicationException {@link Exception}
     */
    UserDTO findById(Long id) throws ApplicationException;

    /**
     * Affiche une liste d'utilisateurs en focntion des critères du filtre passé en paramètre
     *
     * @param pagingAndSort Critère de filter et pagination
     * @return PagedUserDTO DTO spécifique aux listes retournées
     * @throws ApplicationException {@link Exception}
     */
    PagedUserDTO findAll(Pageable pagingAndSort) throws ApplicationException;

    /**
     * Recherche un User avec son nom d'utilisateur
     *
     * @param username
     * @return UserDTO User convertit via sa DTO
     * @throws ApplicationException {@link Exception}
     */
    UserDTO findByUsername(String username) throws ApplicationException;

    /**
     * Créé un nouvel utilisateur et l'enregistre dans la bade de donnée
     *
     * @param request DTO de type requête contenant les données User à enregistrer
     * @return UserDTO User convertit via sa DTO
     * @throws ApplicationException {@link ApplicationException}
     */
    UserDTO create(UserRequestDTO request) throws ApplicationException;

    /**
     * Modifie l'utilisateur dont l'id a été passé en paramètre en donnant les valeur contenu dans le second paramètre
     *
     * @param id      id de l'utilisateur à mettre à jour
     * @param request DTO de type requête contenant les nouvelles données User à enregistrer
     * @return UserDTO User convertit via sa DTO
     * @throws ApplicationException {@link ApplicationException}
     */
    UserDTO update(Long id, UserRequestDTO request) throws ApplicationException;

    /**
     * Supprime l'utilisateur dont l'id a été passé en paramètre
     *
     * @param id id User à supprimer
     * @throws ApplicationException {@link ApplicationException}
     */
    void delete(Long id) throws ApplicationException;
}
