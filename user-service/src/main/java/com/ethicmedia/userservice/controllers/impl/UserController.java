package com.ethicmedia.userservice.controllers.impl;

import com.ethicmedia.userservice.controllers.UserApi;
import com.ethicmedia.userservice.dtos.ApiResponseDTO;
import com.ethicmedia.userservice.dtos.PagedUserDTO;
import com.ethicmedia.userservice.dtos.UserRequestDTO;
import com.ethicmedia.userservice.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Order;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequiredArgsConstructor
public class UserController implements UserApi {

    private static final String SUCCESS = "success";
    private final UserService userService;
    @Value("${api.config.path}")
    private String apiPath;

    @Override
    public ResponseEntity<Object> findById(Long id) {

        return ApiResponseDTO.responseBuilder(SUCCESS,
                "Resource retrieved successfully.",
                HttpStatus.OK,
                HttpStatus.OK.value(),
                userService.findById(id));
    }

    @Override
    public ResponseEntity<Object> findAll(String username,
                                          int page,
                                          int size,
                                          String[] sorts) {
        List<Sort.Order> orders = new ArrayList<>();
        if (sorts[0].contains(",")) {
            // will sort more than 2 fields
            // sortOrder="field, direction"
            for (String sortOrder : sorts) {
                String[] sort = sortOrder.split(",");

                orders.add(getSortDirection(sort[0], sort.length > 1 ? sort[1] : "asc"));
            }
        } else {
            // sort=[field, direction]
            orders.add(getSortDirection(sorts[0], sorts.length > 1 ? sorts[1] : "asc"));
        }
        Pageable pagingSort = PageRequest.of(page, size, Sort.by(orders));
        PagedUserDTO userDTOS = userService.findAll(pagingSort);
        return ApiResponseDTO.responseBuilder(
                SUCCESS,
                " Resource(s) retrieved successfully.",
                HttpStatus.OK,
                HttpStatus.OK.value(),
                userDTOS);
    }

    private Order getSortDirection(String field, String direction) {
        Sort.Direction dire = direction.equalsIgnoreCase("desc") ? Sort.Direction.DESC : Sort.Direction.ASC;
        return new Order(dire, field);
    }

    @Override
    public ResponseEntity<Object> findByUsername(String username) {
        return ApiResponseDTO.responseBuilder(
                SUCCESS,
                "Resource retrieved successfully.",
                HttpStatus.OK,
                HttpStatus.OK.value(),
                userService.findByUsername(username));
    }

    @Override
    public ResponseEntity<Object> create(UserRequestDTO request) {
        return ApiResponseDTO.responseBuilder(
                SUCCESS,
                "Resource created successfully.",
                HttpStatus.CREATED,
                HttpStatus.CREATED.value(),
                userService.create(request));
    }

    @Override
    public ResponseEntity<Object> update(Long id, UserRequestDTO request) {
        return ApiResponseDTO.responseBuilder(
                SUCCESS,
                "Resource updated successfully.",
                HttpStatus.OK,
                HttpStatus.OK.value(),
                userService.update(id, request));
    }

    @Override
    public ResponseEntity<Object> delete(Long id) {
        userService.delete(id);
        return ApiResponseDTO.responseBuilder(
                SUCCESS,
                "Resource deleted successfully.",
                HttpStatus.OK,
                HttpStatus.OK.value(),
                null);
    }


}
