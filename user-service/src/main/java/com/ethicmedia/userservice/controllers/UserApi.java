package com.ethicmedia.userservice.controllers;

/**
 *
 */

import com.ethicmedia.userservice.dtos.UserRequestDTO;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



@RequestMapping("${api.config.path}")
public interface UserApi {

    /**
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    ResponseEntity<Object> findById(@PathVariable(name = "id") Long id);

    /**
     * @param username
     * @param page
     * @param size
     * @param sort
     * @return
     */
    @GetMapping
    ResponseEntity<Object> findAll(@RequestParam(required = false) String username,
                                   @RequestParam(defaultValue = "0") int page,
                                   @RequestParam(defaultValue = "10") int size,
                                   @RequestParam(defaultValue = "id,desc") String[] sort);

    /**
     * @param username
     * @return
     */
    @GetMapping("/by-username/{username}")
    ResponseEntity<Object> findByUsername(@PathVariable(name = "username") String username);

    /**
     * @param request
     * @return
     */
    @PostMapping
    ResponseEntity<Object> create(@Valid @RequestBody UserRequestDTO request);

    /**
     * @param id
     * @param request
     * @return
     */
    @PutMapping("/{id}")
    ResponseEntity<Object> update(@PathVariable(name = "id") @Valid @Min(1) Long id,
                                  @RequestBody @Valid UserRequestDTO request);

    /**
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    ResponseEntity<Object> delete(@NotBlank @Min(1) @PathVariable(name = "id") Long id);
}
