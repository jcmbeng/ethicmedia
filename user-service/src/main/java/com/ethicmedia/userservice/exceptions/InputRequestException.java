package com.ethicmedia.userservice.exceptions;

import com.ethicmedia.userservice.enums.ErrorCodes;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Map;


public class InputRequestException extends RuntimeException {


    /*private final ErrorCodes errorCode;
    private final String message;
    private final HttpStatus httpStatus;
    private final List<Map<String, String>> errors;*/

    @Getter
    private HttpStatus httpStatus;

    @Getter
    private ErrorCodes errorCode;

    @Getter
    private List<Map<String, String>> errors;

    public InputRequestException(String message) {
        super(message);
    }

    public InputRequestException(String message, ErrorCodes errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public InputRequestException( ErrorCodes errorCode, String message, List<Map<String, String>> errors) {
        super(message);
        this.errorCode = errorCode;
        this.errors = errors;
    }

}