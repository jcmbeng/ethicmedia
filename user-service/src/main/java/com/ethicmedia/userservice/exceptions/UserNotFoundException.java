package com.ethicmedia.userservice.exceptions;

import com.ethicmedia.userservice.enums.ErrorCodes;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Getter
@NoArgsConstructor
public class UserNotFoundException extends RuntimeException {


}