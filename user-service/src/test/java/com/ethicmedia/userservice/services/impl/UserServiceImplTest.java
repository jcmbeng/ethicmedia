package com.ethicmedia.userservice.services.impl;

import com.ethicmedia.userservice.dtos.PagedUserDTO;
import com.ethicmedia.userservice.dtos.UserDTO;
import com.ethicmedia.userservice.dtos.UserRequestDTO;
import com.ethicmedia.userservice.models.User;
import com.ethicmedia.userservice.repositories.UserRepository;
import com.ethicmedia.userservice.services.UserService;
import jakarta.inject.Inject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.*;


class UserServiceImplTest {


    @Mock
    private UserRepository userRepository;
    private UserService userService;
    private UserRequestDTO userRequestDTO;

    private UserRequestDTO userUpdateRequestDTO;
    private UserDTO userDTO;

    private User user;

    private AutoCloseable autoCloseable;

    private Page<User> users;

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        userService = new UserServiceImpl(userRepository);
        //Given
        userRequestDTO = UserRequestDTO.builder()
                .username("user123")
                .password("E_pwd123A.")
                .build();



        user = User.builder()
                .id(1L)
                .username("user123")
                .password("E_pwd123A.")
                .build();

        userUpdateRequestDTO = UserRequestDTO.builder()
                .username("user321")
                .password("A_Zwd1oHA.")
                .build();




    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void test_findById() {
        //Some given are defined in setUp method
        long userId = 1L;

        //When
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        UserDTO byId = userService.findById(userId);
        assertThat("user123").isEqualTo(byId.getUsername());
        assertThat(user.getId()).isEqualTo(byId.getId());

        // Verify that the delete method was called on the userRepository
        verify(userRepository, times(1)).findById(any(Long.class));
    }

    @Test
    void test_findAll() {
       /* //Some given are defined in setUp method
        List<Sort.Order> orders = new ArrayList<>();
        Pageable pagingSort = PageRequest.of(0, 1, Sort.by(new Sort.Order(Sort.Direction.ASC, "username")));
        long userId = 1L;

        //When
        when(userRepository.save(any(User.class))).thenReturn(user);
        when(userRepository.findAll(pagingSort)).thenReturn();

        System.out.println(users.getTotalElements());



        PagedUserDTO pagedUserDTO = userService.findAll(pagingSort);
        assertThat(users.isEmpty()).isFalse();
        assertThat("user123").isEqualTo(pagedUserDTO.getUsers().get(0).getUsername());
        assertThat(user.getId()).isEqualTo(pagedUserDTO.getUsers().get(0).getId());

        // Verify that the delete method was called on the userRepository
        verify(userRepository, times(1)).findById(any(Long.class));*/
    }

    @Test
    void test_findByUsername() {
        //Some given are defined in setUp method
        String username = "user123";

        //When
        when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));
        UserDTO byUsername = userService.findByUsername(username);
        assertThat("user123").isEqualTo(byUsername.getUsername());
        assertThat(user.getId()).isEqualTo(byUsername.getId());

        // Verify that the delete method was called on the userRepository
        verify(userRepository, times(1)).findByUsername(any(String.class));
    }

    @Test
    void test_create() {

        //Given are defined in setUp method

        //When
        when(userRepository.save(any(User.class))).thenReturn(user);
        userDTO = userService.create(userRequestDTO);

        //Then
        assertThat(userDTO.getId()).isEqualTo(user.getId());
        assertThat(userDTO.getUsername()).isEqualTo(user.getUsername());
        assertThat(userRequestDTO.getPassword()).isEqualTo(user.getPassword());

        // Verify that the save method was called on the userRepository
        verify(userRepository, times(1)).save(any(User.class));

    }

    @Test
    void test_update() {
        //Some given are defined in setUp method
        long userId = 1L;
        user.setId(userId);
        //When
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(userRepository.save(any(User.class))).thenReturn(user);
        userDTO = userService.update(user.getId(), userUpdateRequestDTO);

        //Then
        assertThat(userDTO.getId()).isEqualTo(user.getId());
        assertThat(userDTO.getUsername()).isEqualTo(userUpdateRequestDTO.getUsername());

        // Verify that the save method was called on the userRepository
        verify(userRepository, times(1)).save(any(User.class));
    }

    @Test
    void test_delete() {
        //Some given are defined in setUp method
        long userId = 1L;
        //When
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        userService.delete(user.getId());
        assertThat(userRepository.findById(user.getId()).isEmpty());

        // Verify that the delete method was called on the userRepository
        verify(userRepository, times(1)).delete(any(User.class));
    }
}