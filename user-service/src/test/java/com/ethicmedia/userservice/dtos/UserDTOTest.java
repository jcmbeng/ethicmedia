package com.ethicmedia.userservice.dtos;

import com.ethicmedia.userservice.models.User;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;

class UserDTOTest {


    private User user;


    private UserDTO userDTO;


    @Test
    void test_fromEntityToDTO () {
        user = mock (User.class);
        when (user.getId ()).thenReturn (10000L);
        when (user.getUsername ()).thenReturn ("1234USER");

        userDTO = UserDTO.fromEntityToDTO (user, "");

        assertThat (userDTO instanceof UserDTO).isTrue ();
        assertThat (userDTO.getUsername ()).isEqualTo ("1234USER");
        assertThat (userDTO.getId ()).isEqualTo (10000L);

    }
}