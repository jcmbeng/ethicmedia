package com.ethicmedia.userservice.dtos;

import com.ethicmedia.userservice.models.User;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class UserRequestDTOTest {

    private UserRequestDTO userRequestDTO;

    private User user;

    @Test
    void test_fromDtoToEntity () {
        userRequestDTO = mock (UserRequestDTO.class);
        when (userRequestDTO.getUsername ()).thenReturn ("123456USR");
        when (userRequestDTO.getPassword ()).thenReturn ("123456PWD");

        user = UserRequestDTO.fromDtoToEntity (userRequestDTO);

        assertThat (user instanceof User).isTrue ();
        assertThat (user.getUsername ()).isEqualTo ("123456USR");
        assertThat (user.getPassword ()).isEqualTo ("123456PWD");

    }
}