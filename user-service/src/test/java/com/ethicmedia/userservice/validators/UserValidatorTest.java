package com.ethicmedia.userservice.validators;

import com.ethicmedia.userservice.dtos.UserRequestDTO;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;


import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


class UserValidatorTest {

    @ParameterizedTest(name = "#{index} - Run test with username = {0}")
    @MethodSource("validUserRequestDTO")
    void test_validate(UserRequestDTO dto) {
        assertTrue(UserValidator.validate(dto).size() == 0);
    }


    @ParameterizedTest(name = "#{index} - Run test with username = {0}")
    @MethodSource("invalidPasswordProvider")
    void test_invalidatePassword(String password) {
        assertTrue(!UserValidator.validatePassword(password).isEmpty());
    }

    @ParameterizedTest(name = "#{index} - Run test with username = {0}")
    @MethodSource("validPasswordProvider")
    void test_validatePassword(String password) {
        assertTrue(UserValidator.validatePassword(password).isEmpty());
    }

    @ParameterizedTest(name = "#{index} - Run test with username = {0}")
    @MethodSource("validUsernameProvider")
    void test_validateUsername(String username) {
        assertTrue(UserValidator.validateUsername(username));
    }

    @ParameterizedTest(name = "#{index} - Run test with username = {0}")
    @MethodSource("invalidUsernameProvider")
    void test_invalidateUsername(String username) {
        assertFalse(UserValidator.validateUsername(username));
    }

    static Stream<String> validUsernameProvider() {
        return Stream.of(
                "jcmbeng",
                "jcm123",
                "Java.145");
    }

    static Stream<UserRequestDTO> validUserRequestDTO() {
        return Stream.of(
                UserRequestDTO.builder().username("jcmbeng").password(".123Ag_Ugtp").build(),
                UserRequestDTO.builder().username("user123").password("P.123Ag_qS").build(),
                UserRequestDTO.builder().username("momoralf").password(".PAw78/pL4").build(),
                UserRequestDTO.builder().username("rugal123").password(".Omega@1998z").build()
        );
    }

    static Stream<String> invalidUsernameProvider() {
        return Stream.of(".jcmbeng",
                "jcm.123.",
                ".Java.145",
                " ",
                "_7PMk");                        // empty
    }

    static Stream<String> validPasswordProvider() {
        return Stream.of(
                "_Pas@/1972i",
                "Ajcm123/",
                "Java.145");
    }

    static Stream<String> invalidPasswordProvider() {
        return Stream.of(".jcmbeng",
                "jcm.123.",
                "abcd784",
                " ",
                "_7PM");                        // empty
    }
}