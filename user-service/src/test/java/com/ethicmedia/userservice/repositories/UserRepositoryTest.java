package com.ethicmedia.userservice.repositories;

import com.ethicmedia.userservice.models.User;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;


import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
@DataJpaTest

class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;
    private User user;

    @BeforeEach
    void setUp() {
        user = User.builder()
                .username("user123")
                .password("pwd123")
                .build();
        userRepository.save(user);
    }

    @AfterEach
    void tearDown() {
        user = null;
        userRepository.deleteAll();
    }

    // Test case SUCCESS
    @Test
    void test_findByUsernameOrSuccess() {

        Optional<User> byUsername = userRepository.findByUsername("user123");
        assertThat(byUsername.isPresent());
        assertThat(byUsername.get().getUsername()).isEqualTo("user123");

    }

    // Test case FAILURE
    @Test
    void test_findByUsernameOrFail() {
        Optional<User> byUsername = userRepository.findByUsername("user123F");
        assertThat(byUsername.isEmpty());
    }

    @Test
    void test_existsUserByUsername() {

        boolean isExistsUsername = userRepository.existsUserByUsername("user123");
        assertThat(isExistsUsername);

    }
}